package com.shoppyng.gestion.panier.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Produit {

	String name;
	Double montant;

	public Produit(String name, Double montant) {
		this.name = name;
		this.montant = montant;
	}

}
