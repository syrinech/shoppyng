package com.shoppyng.gestion.panier.model;

import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Panier {

	Acheteur acheteur;
	Map<Produit, Integer> produits;

	public Panier(Acheteur acheteur, Map<Produit, Integer> produits) {
		this.acheteur = acheteur;
		this.produits = produits;
	}

	@Override
	public String toString() {
		return afficherProduits();
	}
	/*
	 * M�thode pour afficher la liste des produits contenus dans le panier
	 */

	public String afficherProduits() {
		String afficheProduit = "{";
		for (Produit p : produits.keySet()) {
			afficheProduit += "Produit: " + p.getName() + ", Quantit�: " + produits.get(p);
			afficheProduit += System.getProperty("line.separator");
		}
		return afficheProduit += "}";
	}

	/*
	 * M�thode pour calculer le montant total des produits contenus dans le panier
	 */
	public Double getMontantTotal() {
		Double montant = 0.0;
		for (Produit p : produits.keySet()) {
			montant += (p.getMontant() * produits.get(p));
		}
		return montant;
	}
}
