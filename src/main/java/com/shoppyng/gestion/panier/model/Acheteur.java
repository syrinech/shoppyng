package com.shoppyng.gestion.panier.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Acheteur  {
	String name;

	public Acheteur(String name) {
		this.name = name;
	}
}
