package com.shoppyng.gestion.panier;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.shoppyng.gestion.panier.model.Acheteur;
import com.shoppyng.gestion.panier.model.Panier;
import com.shoppyng.gestion.panier.model.Produit;

public class GestionPanier {
	protected static final Logger logger = LoggerFactory.getLogger(GestionPanier.class);
	static java.text.DecimalFormat df = new java.text.DecimalFormat("0.##");

	public static void main(String[] args) {
		logger.info("------------ Begin Shopping ------------");
		Acheteur acheteur = new Acheteur("Fran�ois");
		Map<Produit, Integer> produits = new HashMap<Produit, Integer>();
		Panier panier = new Panier(acheteur, produits);
		logger.info("Etat du panier {}", panier);
		logger.info("L acheteur {} commence a remplir son panier", acheteur.getName());
		logger.info("Ajout premier produit");
		Produit produit1 = new Produit("T-shirt", 30.5);
		produits.put(produit1, 2);
		logger.info("Etat du panier {}", panier);
		logger.info("Ajout deuxieme produit");
		Produit produit2 = new Produit("Robe", 20.98);
		panier.getProduits().put(produit2, 1);
		logger.info("Etat du panier {}", panier);
		logger.info("Suppression quantite premier produit");
		panier.getProduits().put(produit1, 1);
		logger.info("Etat du panier {}", panier);
		logger.info("Montant total du panier {}", df.format(panier.getMontantTotal()));
		logger.info("------------ End Shopping ------------");

	}
}
