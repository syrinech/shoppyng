package com.shoppyng.gestion.panier.model;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.shoppyng.gestion.panier.model.Acheteur;
import com.shoppyng.gestion.panier.model.Panier;
import com.shoppyng.gestion.panier.model.Produit;

public class PanierTest {

	Panier panier;
	Map<Produit, Integer> produits;
	Acheteur acheteur;
	Produit produit;
	java.text.DecimalFormat df = new java.text.DecimalFormat("0.##");

	@Before
	public void init() {
		acheteur = new Acheteur("Bertil");
		produit = new Produit("chemise", 14.66);
		produits = new HashMap<Produit, Integer>();
		panier = new Panier(acheteur, produits);
	}

	@Test
	public void testMontantAfterAddProduct() {
		panier.getProduits().put(produit, 5);
		assertEquals(df.format(panier.getMontantTotal()), df.format(Double.valueOf(73.3)));
	}

	@Test
	public void testMontantAfterDeleteProduct() {
		panier.getProduits().put(produit, 2);
		assertEquals(df.format(panier.getMontantTotal()), df.format(Double.valueOf(29.32)));
	}
}
