package com.shoppyng.gestion.panier.model;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.shoppyng.gestion.panier.model.Acheteur;

public class AcheteurTest {
	Acheteur acheteur;

	@Before
	public void init() {
		acheteur = new Acheteur("Jean-michel");
	}

	@Test
	public void testName() {
		assertEquals(acheteur.getName(), "Jean-michel");

	}
}
