package com.shoppyng.gestion.panier.model;

import static org.junit.Assert.assertNotEquals;

import org.junit.Before;
import org.junit.Test;

import com.shoppyng.gestion.panier.model.Produit;

public class ProduitTest {
	Produit produit;

	@Before
	public void init() {
		produit = new Produit("Manteau", 59.67);
	}

	@Test
	public void testMontant() {
		assertNotEquals(produit.getMontant(), 71.9);
	}
}
